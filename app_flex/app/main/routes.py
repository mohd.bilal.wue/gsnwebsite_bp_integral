from flask import render_template
from app.main import bp

@bp.route('/home', methods=['GET', 'POST'])
def home():
    return render_template('main/home.html')
