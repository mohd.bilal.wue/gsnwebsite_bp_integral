from flask import render_template
from app.index import bp

@bp.route('/', methods=['GET'])
def index():
    return render_template('index/landing_page.html')
