'''
Created on Oct 28, 2019

@author: mob33uk
'''

import os
from dotenv import load_dotenv

basedir = os.path.abspath(os.path.dirname(__file__))
load_dotenv(os.path.join(basedir, '.env'))


class Config(object):
    SECRET_KEY = os.getenv("FLASK_SECRET_KEY_TIM")
    ADMINS = ['mohd.bilal@uni-wuerzburg.de']
    LANGUAGES = ['en']


